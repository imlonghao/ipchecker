package main

import (
	"fmt"
	"net"
	"strings"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	ping "github.com/sparrc/go-ping"
)

type requestJSON struct {
	IP   string `json:"ip"`
	Port string `json:"port"`
}

func checkICMP(res *bool, wg *sync.WaitGroup, ip string) {
	pinger, err := ping.NewPinger(ip)
	if err != nil {
		*res = false
		wg.Done()
		return
	}
	pinger.SetPrivileged(true)
	pinger.Timeout = 1 * time.Second
	pinger.Run()
	stats := pinger.Statistics()
	*res = stats.PacketsRecv != 0
	wg.Done()
}

func checkTCP(res *bool, wg *sync.WaitGroup, ip string, port string) {
	_, err := net.DialTimeout("tcp", fmt.Sprintf("%s:%s", ip, port), 1*time.Second)
	if err != nil && strings.Contains(err.Error(), "i/o timeout") {
		*res = false
		wg.Done()
		return
	}
	*res = true
	wg.Done()
}

func main() {
	r := gin.Default()
	r.POST("/", func(c *gin.Context) {
		var request requestJSON
		if err := c.ShouldBindJSON(&request); err != nil {
			c.JSON(400, gin.H{"error": err.Error()})
			return
		}
		var (
			icmp bool
			tcp  bool
			wg   sync.WaitGroup
		)
		wg.Add(2)
		go checkICMP(&icmp, &wg, request.IP)
		go checkTCP(&tcp, &wg, request.IP, request.Port)
		wg.Wait()
		c.JSON(200, gin.H{
			"icmp": icmp,
			"tcp":  tcp,
		})
	})
	r.Run()
}
