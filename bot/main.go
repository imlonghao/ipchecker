package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	jsoniter "github.com/json-iterator/go"
	tb "gopkg.in/tucnak/telebot.v2"
)

type requestJSON struct {
	IP   string `json:"ip"`
	Port string `json:"port"`
}

type responseJSON struct {
	Icmp bool `json:"icmp"`
	TCP  bool `json:"tcp"`
}

func send(icmp *bool, tcp *bool, e *bool, wg *sync.WaitGroup, endpoint string, j []byte) {
	client := http.Client{}
	req, _ := http.NewRequest("POST", endpoint, bytes.NewBuffer(j))
	response, err := client.Do(req)
	if err != nil {
		*icmp = false
		*tcp = false
		*e = true
		wg.Done()
		return
	}
	buffer := new(bytes.Buffer)
	buffer.ReadFrom(response.Body)
	defer response.Body.Close()
	var rjson responseJSON
	json.Unmarshal(buffer.Bytes(), &rjson)
	*icmp = rjson.Icmp
	*tcp = rjson.TCP
	*e = false
	wg.Done()
}

func main() {
	bot, err := tb.NewBot(tb.Settings{
		Token:  os.Getenv("TOKEN"),
		Poller: &tb.LongPoller{Timeout: 10 * time.Second},
	})
	if err != nil {
		panic(err)
	}
	bot.Handle("/q", func(m *tb.Message) {
		ipport := strings.Split(m.Payload, ":")
		ip := net.ParseIP(ipport[0]).To4()
		if ip == nil {
			bot.Send(m.Sender, "/q ipv4[:port]", &tb.SendOptions{
				ReplyTo: m,
			})
			return
		}
		var port int
		if len(ipport) == 1 {
			port = 80
		} else {
			port, err = strconv.Atoi(ipport[1])
			if err != nil || port < 0 || port > 65535 {
				bot.Send(m.Sender, "/q ipv4[:port]", &tb.SendOptions{
					ReplyTo: m,
				})
				return
			}
		}
		request := new(requestJSON)
		request.IP = ip.String()
		request.Port = strconv.Itoa(port)
		requestjson, _ := jsoniter.Marshal(&request)
		var (
			cnicmp  bool
			cntcp   bool
			cnerror bool
			usicmp  bool
			ustcp   bool
			userror bool
			wg      sync.WaitGroup
		)
		wg.Add(2)
		go send(&cnicmp, &cntcp, &cnerror, &wg, os.Getenv("CNURL"), requestjson)
		go send(&usicmp, &ustcp, &userror, &wg, os.Getenv("USURL"), requestjson)
		wg.Wait()
		text := fmt.Sprintf("目标 - %s:%s\n\n", request.IP, request.Port)
		if cnerror && !userror {
			text = fmt.Sprintf("%s国外 TCP - %t\n国外 ICMP - %t\n\n国内节点暂不可用", text, ustcp, usicmp)
		} else if !cnerror && userror {
			text = fmt.Sprintf("%s国内 TCP - %t\n国内 ICMP - %t\n\n国外节点暂不可用", text, cntcp, cnicmp)
		} else if cnerror && userror {
			text = fmt.Sprintf("%s国内/国外节点暂不可用", text)
		} else {
			text = fmt.Sprintf("%s国内 TCP - %t\n国外 TCP - %t\n国内 ICMP - %t\n国外 ICMP - %t", text, cntcp, ustcp, cnicmp, usicmp)
		}
		text = strings.Replace(text, "true", "✅", -1)
		text = strings.Replace(text, "false", "❎", -1)
		bot.Send(m.Sender, text, &tb.SendOptions{
			ReplyTo: m,
		})
	})
	bot.Start()
}
