module gitlab.com/imlonghao/ipchecker/bot

require (
	github.com/json-iterator/go v1.1.5
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	gopkg.in/tucnak/telebot.v2 v2.0.0-20190128004241-7eeea8f32a9b
)
